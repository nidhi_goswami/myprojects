namespace FizzBuzz.DependencyResolution
{
    using FizzBuzz.Services;
    using StructureMap;

    public class DefaultRegistry : Registry
    {
        public DefaultRegistry()
        {
            this.Scan(
                scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.With(new ControllerConvention());
                });
            this.For<IRule>().Use<FizzBuzzRule>();
            this.For<IRule>().Use<FizzRule>();
            this.For<IRule>().Use<BuzzRule>();
            this.For<IFizzBuzzService>().Use<FizzBuzzService>();
        }
    }
}
