﻿namespace FizzBuzz.Services
{
    using System;

    public class FizzRule : IRule, IDateTimeService
    {
        private readonly IDateTimeService dateTimeService;

        public FizzRule(IDateTimeService dateTimeService)
        {
            this.dateTimeService = dateTimeService;
        }

        public bool IsMatch(int number)
        {
            return number % 3 == 0;
        }

        public string Execute()
        {
            var dayOfWeek = this.dateTimeService.GetDateTimeNow().DayOfWeek;
            if (dayOfWeek == DayOfWeek.Wednesday)
            {
                return "wizz";
            }
            else
            {
                return "fizz";
            }
        }

        public DateTime GetDateTimeNow()
        {
            throw new NotImplementedException();
        }
    }
}