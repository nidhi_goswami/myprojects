﻿namespace FizzBuzz.Services
{
    using System;

    public class DateTimeService : IDateTimeService
    {
        public DateTime GetDateTimeNow()
        {
            return DateTime.Now;
        }
    }
}