﻿namespace FizzBuzz.Services
{
    using System.Collections.Generic;

    public interface IFizzBuzzService
    {
        IList<string> GetFizzBuzzNumbers(int input);
    }
}
