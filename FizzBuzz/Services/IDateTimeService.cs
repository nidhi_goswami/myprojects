﻿namespace FizzBuzz.Services
{
    using System;

    public interface IDateTimeService
    {
        DateTime GetDateTimeNow();
    }
}
