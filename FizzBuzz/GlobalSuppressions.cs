﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "not applied", Scope = "namespace", Target = "~N:FizzBuzz.Services")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "not applied", Scope = "namespace", Target = "~N:FizzBuzz.DependencyResolution")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "not applied", Scope = "namespace", Target = "~N:FizzBuzz")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "not applied", Scope = "namespace", Target = "~N:FizzBuzz.DependencyResolution")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "not applied")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "not applied", Scope = "member", Target = "~M:FizzBuzz.Models.FizzbuzzViewModel.#ctor")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "type", Target = "~T:FizzBuzz.Models.FizzbuzzViewModel")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "type", Target = "~T:FizzBuzz.MvcApplication")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1649:File name should match first type name", Justification = "not applied", Scope = "type", Target = "~T:FizzBuzz.MvcApplication")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "type", Target = "~T:FizzBuzz.DependencyResolution.StructureMapDependencyScope")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "not applied", Scope = "member", Target = "~P:FizzBuzz.DependencyResolution.StructureMapDependencyScope.CurrentNestedContainer")]
[assembly: SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1500:Braces for multi-line statements should not share line", Justification = "not applied", Scope = "member", Target = "~M:FizzBuzz.DependencyResolution.StructureMapDependencyScope.CreateNestedContainer")]
[assembly: SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1500:Braces for multi-line statements should not share line", Justification = "not applied", Scope = "member", Target = "~M:FizzBuzz.DependencyResolution.StructureMapDependencyScope.DoGetInstance(System.Type,System.String)~System.Object")]
[assembly: SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1500:Braces for multi-line statements should not share line", Justification = "not applied", Scope = "member", Target = "~M:FizzBuzz.DependencyResolution.StructureMapDependencyScope.DisposeNestedContainer")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "not applied")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "member", Target = "~M:FizzBuzz.Services.DateTimeService.GetDateTimeNow~System.DateTime")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "member", Target = "~M:FizzBuzz.Services.FizzBuzzService.#ctor(System.Collections.Generic.IList{FizzBuzz.Services.IRule})")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "type", Target = "~T:FizzBuzz.Constants.FizzBuzzConstants")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "type", Target = "~T:FizzBuzz.Services.DateTimeService")]
[assembly: SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1508:Closing braces should not be preceded by blank line", Justification = "not applied", Scope = "member", Target = "~M:FizzBuzz.Services.FizzBuzzService.#ctor(System.Collections.Generic.IList{FizzBuzz.Services.IRule})")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "type", Target = "~T:FizzBuzz.Services.FizzBuzzService")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "<Pending>", Scope = "type", Target = "~T:FizzBuzz.Services.FizzBuzzRule")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "<Pending>", Scope = "type", Target = "~T:FizzBuzz.Services.FizzRule")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "<Pending>", Scope = "type", Target = "~T:FizzBuzz.Services.BuzzRule")]
