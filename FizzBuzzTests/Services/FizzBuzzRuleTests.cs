﻿namespace FizzBuzzTests.Services
{
    using FizzBuzz.Services;
    using FluentAssertions;
    using Xunit;

    public class FizzBuzzRuleTests
    {
        [Theory]
        [InlineData(1, false)]
        [InlineData(15, true)]

        public void IsMatchShouldReturnValueBasedOnInput(int input, bool expectated)
        {
            // Arrange
            var rule = new FizzBuzzRule();

            // Act
            var isMatch = rule.IsMatch(input);

            // Assert
            isMatch.Should().Be(expectated);
        }

        [Fact]
        public void ExecuteShouldReturnValueFizzBuzz()
        {
            // Arrange
            var rule = new FizzBuzzRule();

            // Act
            var execute = rule.Execute();

            // Assert
            execute.Should().Be("fizz buzz");
        }
    }
}
