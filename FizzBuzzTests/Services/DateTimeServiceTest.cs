﻿namespace FizzBuzzTests.Services
{
    using System;
    using FizzBuzz.Services;
    using FluentAssertions;
    using Moq;
    using Xunit;

    public class DateTimeServiceTest
    {
        [Fact]
        public void WhetherGetDateTimeReturnsCurrentInput()
        {
            // Arrange
            var dateTimeService = new DateTimeService();

            // Act
            var service = dateTimeService.GetDateTimeNow();

            // Assert
            service.ToShortDateString().Should().NotBe(new DateTime().ToShortDateString());
        }
    }
}
