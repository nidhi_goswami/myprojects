﻿namespace FizzBuzzTests.Controllers
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzz.Controllers;
    using FizzBuzz.Models;
    using FizzBuzz.Services;
    using FluentAssertions;
    using Moq;
    using PagedList;
    using Xunit;

    public class FizzBuzzControllerTests
    {
        private Mock<IFizzBuzzService> mockFizzBuzzService;

        public FizzBuzzControllerTests()
        {
            this.mockFizzBuzzService = new Mock<IFizzBuzzService>();
        }

        [Fact]
        public void WhetherIndexLoadsView()
        {
            // Arrange
            var controller = new FizzBuzzController(this.mockFizzBuzzService.Object);
            this.mockFizzBuzzService.Setup(x => x.GetFizzBuzzNumbers(It.IsAny<int>())).Returns(new List<string>());
            var expectedModel = new FizzbuzzViewModel();

            // Act
            var expectedView = controller.Index();

            // Assert
            var result = expectedView as ViewResult;
            result.ViewName.Should().Be("Index");
            result.Model.Should().BeEquivalentTo(result.Model);
        }

        [Theory]
        [InlineData(-1, 1)]
        [InlineData(2, 2)]
        public void WhetherIndexReturnToExpectedPage(int pageNumber, int expectedPageNumber)
        {
            // Arrange
            var controller = new FizzBuzzController(this.mockFizzBuzzService.Object);
            this.mockFizzBuzzService.Setup(x => x.GetFizzBuzzNumbers(It.IsAny<int>())).Returns(new List<string>());
            var expectedModel = new FizzbuzzViewModel { FizzBuzzNumbers = new List<string>().ToPagedList(expectedPageNumber, 1), Input = 1 };

            // Act
            var expectedView = controller.Index(pageNumber, 1);

            // Assert
            var result = expectedView as ViewResult;
            result.ViewName.Should().Be("Index");
            result.Model.Should().BeEquivalentTo(expectedModel);
        }

        [Fact]
        public void WhetherModelsUpdatedWithExpectedFizzBuzzNumbers()
        {
            // Arrange
            this.mockFizzBuzzService.Setup(x => x.GetFizzBuzzNumbers(It.IsAny<int>())).Returns(new List<string> { "1" });
            var controller = new FizzBuzzController(this.mockFizzBuzzService.Object);

            // Act
            var result = controller.Index(new FizzBuzz.Models.FizzbuzzViewModel { Input = 1 });

            // Assert
            var output = result as ViewResult;
            output.ViewName.Should().Be("Index");
            output.Model.Should().BeEquivalentTo(new FizzbuzzViewModel
            {
                Input = 1,
                FizzBuzzNumbers = new PagedList<string>(new List<string> { "1" }, 1, 1),
            });
            this.mockFizzBuzzService.Verify(x => x.GetFizzBuzzNumbers(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void WhetherModelsWillNotBeUpdatedWithExpectedFizzBuzzNumbers()
        {
            // Arrange
            this.mockFizzBuzzService.Setup(x => x.GetFizzBuzzNumbers(It.IsAny<int>())).Returns(new List<string> { "1" });
            var controller = new FizzBuzzController(this.mockFizzBuzzService.Object);
            controller.ModelState.AddModelError("key", "error message");

            // Act
            var result = controller.Index(new FizzBuzz.Models.FizzbuzzViewModel { Input = -1 });

            // Assert
            var output = result as ViewResult;
            output.ViewName.Should().Be("Index");
            output.Model.Should().BeEquivalentTo(new FizzbuzzViewModel
            {
                Input = -1,
                FizzBuzzNumbers = new PagedList<string>(new List<string> { }, 1, 1),
            });
            this.mockFizzBuzzService.Verify(x => x.GetFizzBuzzNumbers(It.IsAny<int>()), Times.Never);
        }
    }
}
