﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "not applied", Scope = "namespace", Target = "~N:FizzBuzzTests.Services")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "type", Target = "~T:FizzBuzzTests.Services.DateTimeServiceTest")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1129:Do not use default value type constructor", Justification = "not applied", Scope = "member", Target = "~M:FizzBuzzTests.Services.DateTimeServiceTest.WhetherGetDateTimeReturnsCurrentInput")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "member", Target = "~M:FizzBuzzTests.Services.FizzBuzzRuleTests.IsMatchShouldReturnValueBasedOnInput(System.Int32,System.Boolean)")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "member", Target = "~M:FizzBuzzTests.Services.FizzBuzzRuleTests.ExecuteShouldReturnValueFizzBuzz")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "member", Target = "~M:FizzBuzzTests.Services.FizzBuzzServiceTest.#ctor")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "type", Target = "~T:FizzBuzzTests.Services.FizzRuleTests")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "type", Target = "~T:FizzBuzzTests.Services.FizzBuzzRuleTests")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "type", Target = "~T:FizzBuzzTests.Services.FizzBuzzServiceTest")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "not applied")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "not applied")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "type", Target = "~T:FizzBuzzTests.Controllers.FizzBuzzControllerTests")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "member", Target = "~M:FizzBuzzTests.Services.BuzzRuleTests.IsMatchShouldReturnValueBasedOnInput(System.Int32,System.Boolean)")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:Elements should be documented", Justification = "not applied", Scope = "type", Target = "~T:FizzBuzzTests.Services.BuzzRuleTests")]
